﻿$(function () {

    function BookViewModel() {
        var self = this;
        var currRequest = Cookies.getJSON("CurrRequest");
        self.bookInfo = ko.observable(currRequest);
        self.results = ko.observableArray([]);

        self.GetRooms = function () {
            $.ajax({
                url: url,
                data: { hotelCode: currCode },
                type: 'get',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                    self.results(data);
                }
            });
        };

        self.Increase = function (data, event) {
            if (this.BookedRoom < this.AvailableRoom) {
                this.BookedRoom++;
                this.TotalPrice = this.Price * this.BookedRoom;
                $(event.target).parents('.room-item').find('.price-amount').html(this.TotalPrice);
                $(event.target).parent().find('.room-number-field').attr({
                    "value": this.BookedRoom
                    });
            }
            
        };

        self.Decrease = function (data, event) {
            if (this.BookedRoom > 1) {
                this.BookedRoom--;
                this.TotalPrice = this.Price * this.BookedRoom;
                $(event.target).parents('.room-item').find('.price-amount').html(this.TotalPrice);
                $(event.target).parent().find('.room-number-field').attr({
                    "value": this.BookedRoom
                });
            }

        };
        
        self.ReserveRoom = function() {
            $('body').block({ message: 'Processing..' });
            var data = this;
            $.extend(data, currRequest);
            $.ajax({
                url: url,
                data: data,
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                },
                complete: function () {
                    $('body').unblock();
                    window.location.href = '/BookRoom';
                }
            });
        };

        self.openDetail = function() {
            var data = this;
            Cookies.set('PendingBook', data);
            window.location.href = '/Room/Detail?hc=' + currCode + '&ri=' + data.RoomId + '&rc=' + data.RoomCode;
        };

    }

    var vmodel = new BookViewModel();
    ko.applyBindings(vmodel);
    vmodel.GetRooms();
});