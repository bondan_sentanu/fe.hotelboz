﻿$(function () {


    function ConfirmViewModel() {
        var self = this;
        var uifo = Cookies.getJSON("uifo");
        if (typeof uifo === "undefined") {
            self.fullName = ko.observable('');
            self.email = ko.observable('');
            self.phone = ko.observable('');
        } else {
            self.fullName = ko.observable(uifo.FullName);
            self.email = ko.observable(uifo.EmailAddress);
            self.phone = ko.observable(uifo.Phone);
        }
        self.bookingInfo = ko.observable({});
        self.specialRequest = ko.observable('');

        self.GetBookingInfo = function () {
            $.ajax({
                url: url,
                type: 'get',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                    self.bookingInfo(data);
                }
            });
        };

        self.SubmitBook = function (paymentMethod) {
            $('body').block({ message: 'Processing..' });
            var data = {
                EmailAddress: self.email(),
                Phone: self.phone(),
                FullName: self.fullName(),
                SpecialRequest: self.specialRequest(),
                PaymentMethod: paymentMethod
            };
            Cookies.set('uifo', data);
            $.ajax({
                url: url,
                data: data,
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                    window.location.href = urlPayment;
                },
                complete: function () {
                    window.location.href = urlPayment;
                }
            });
        };


    };

    var vmodel = new ConfirmViewModel();
    ko.applyBindings(vmodel);
    vmodel.GetBookingInfo();
});