$(function () {
    // API SEARCH url
    //var url = 'http://roomboz-api-staging.azurewebsites.net/api/Search';	

    function parseDate(dateString) {
        // from D, dd-M-yy to 2015-02-02
        var split1 = dateString.split(" ");
        var theDate = split1[1].split("-");
        var month = ("JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1) < 10 ? "0" + ("JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1) : ("JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1);

        return (theDate[2] + '-' + month + '-' + theDate[0]);
    }

    function AppViewModelSearch() {
        var self = this;
        self.isLoading = ko.observable(false);
        self.searchPropertyName = ko.observable('');
        self.searchTerm = ko.observable('');
        self.keywordDelayed = ko.computed(self.searchTerm).extend({ throttle: 800 });

        // LOAD HISTORY FROM COOKIE
        var previousHistory = Cookies.getJSON('roombozsearchhistory');

        self.historyList = ko.observableArray(previousHistory);

        self.results = ko.observableArray([]);
        self.keywordDelayed.subscribe(function (newValue) {
            self.isLoading(true);
            $.ajax({
                url: url,
                data: { keyword: self.keywordDelayed() },
                type: 'get',
                success: function (data) {
                    self.results(data);
                },
                complete: function () {
                    self.isLoading(false);
                }
            });
        });

        // click event binding
        self.selectResult = function () {            
            var that = this;
            var theArr = self.historyList();
            
            //check history, if there's history of the item, delete first
            $.each(theArr, function(i) {
                if (this.Code == that.Code) {                    
                    theArr.splice(i,1);
                }
            });            
            
            self.historyList(theArr);
            self.historyList.reverse();
            self.historyList.push(this);
            self.historyList.reverse();

            self.searchPropertyName(this);

            // WRITE HISTORY TO COOKIE
            Cookies.set('roombozsearchhistory', self.historyList());

            // empty the current search
            self.searchTerm('');
            self.results([]);

            //close litebox
            $('.litebox-slide.active .close-litebox').click();
        }

        self.searchHotels = function () {
            var item = self.searchPropertyName();
            var checkin = parseDate($('#check-in-date').val());
            var checkout = parseDate($('#check-out-date').val());
            var duration = (new Date(Date.parse(checkout) - Date.parse(checkin))).getDate() - 1;
            var type = "city";
            switch (item.SearchContentType) {
                case 0:
                    type = "area"; break;
                case 1:
                    type = "area"; break;
                case 2:
                    type = "city"; break;
                case 3:
                    type = "hotel";break;
            }

            var request = {
                DestinationCode: item.Code,
                DestinationType: type,
                CheckinDate: checkin,
                CheckoutDate: checkout,
                Duration: duration,
                IsRetrieveData: true,
                CityName: item.Title
            };

            Cookies.set('CurrRequest', request);
            window.location.href = '/Hotel';

            return false;
        };
    }

    // Activates knockout.js
    ko.applyBindings(new AppViewModelSearch());
});