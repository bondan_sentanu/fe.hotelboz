// KO INFINITE SCROLL SAMPLE: http://jsfiddle.net/bondythegreat/st6b1a7a/

$(function () {
    //var url = 'http://roomboz-api-staging.azurewebsites.net/api/Hotel';				
    var currRequest = Cookies.getJSON("CurrRequest");
    var pageNumber = 1; // current page number..will increase and ajax call until meet totalPageNumber
    var pageOffset = 10; // item per page
    var totalPageNumber = 1; // default, soon we have it from ajax response

    // throw to homepage if cookies is not set
    if (currRequest === undefined) {
        window.location.href = '/';
    }

    var monthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun",
    "Jul", "Aug", "Sep",
    "Oct", "Nov", "Dec"
    ];

    function requestAjaxPagination(self, pageNumber) {
        $('#pagination-hotel-list .loading').addClass('active');
        $.extend(currRequest, {Paging: {Page: pageNumber, PageSize: pageOffset}})
        $.ajax({
            url: url,
            data: currRequest,
            type: 'post',
            xhrFields: {
                withCredentials: true
            },
            success: function (data) {
                //self.results(data);
                var tempResults = self.results();
                ko.utils.arrayPushAll(tempResults, data.List);
                self.results(tempResults);

                // TODO: ambil total page number dari response
                totalPageNumber = Math.ceil(data.TotalRows / pageOffset);

                // ambil cityname untuk dirender di header sebelum iterasi hotel
                self.cityName(data.List[0].CityName);
            },
            complete: function () {
                $('body').unblock();
                $('#pagination-hotel-list .loading').removeClass('active');
            }
        });
    }

    function AppViewModelList() {
        var self = this;

        self.results = ko.observableArray([]);
        self.results.extend({
            infinitescroll: {}
        });
        self.cityName = ko.observable();
        self.bookInfo = ko.observable(currRequest);

        self.durationText = ko.computed(function () {
            var duration = self.bookInfo().Duration;
            var nightText = (duration > 1) ? 'nights' : 'night';
            var CheckinDate = new Date(self.bookInfo().CheckinDate);
            var CheckoutDate = new Date(self.bookInfo().CheckoutDate);

            var newCheckinDate = monthNames[CheckinDate.getMonth()] + ' ' + CheckinDate.getDate() + ',' + CheckinDate.getFullYear();
            var newCheckoutDate = monthNames[CheckoutDate.getMonth()] + ' ' + CheckoutDate.getDate() + ',' + CheckoutDate.getFullYear();

            return (duration + ' ' + nightText + ' (' + newCheckinDate + ' to ' + newCheckoutDate + ')');
        });

        $('body').block({ message: 'Loading..' });
        requestAjaxPagination(self, pageNumber);

        // INFINITE SCROLL RESULTS
        $('#pagination-hotel-list').on('appear', function(event, $all_appeared_elements) {
          // this element is now inside browser viewport, call infinite scroll page2
          console.log('bottom');
          if (pageNumber < totalPageNumber) {
                pageNumber++;
                requestAjaxPagination(self, pageNumber);
          }
        });

        $(window).scroll(function() {  
            
            if (($('#pagination-hotel-list').is_on_screen()) && (pageNumber < totalPageNumber)) {                
               pageNumber++;
               requestAjaxPagination(self, pageNumber);
            }
        });        

        self.SeeDetail = function () {
            window.location.href = '/Hotel/Detail?code=' + this.HotelCode;
        };

        self.SortBy = function(param) {
            $('body').block({ message: 'Loading..' });
            var sortBy = { SortBy: param };
            $.extend(currRequest, sortBy);
            $.ajax({
                url: url,
                data: currRequest,
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                success: function(data) {
                    self.results(data);
                    // ambil cityname untuk dirender di header sebelum iterasi hotel
                    self.cityName(data[0].CityName);
                },
                complete: function() {
                    $('body').unblock();
                }
            });
        };
    }

    // Activates knockout.js
    ko.applyBindings(new AppViewModelList());

});
