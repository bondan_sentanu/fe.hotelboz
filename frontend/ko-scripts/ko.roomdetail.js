﻿$(function () {

    function RoomDetailViewModel() {
        var self = this;
        var currRequest = Cookies.getJSON("CurrRequest");
        var currBook = Cookies.getJSON("PendingBook");
        self.pendingBook = ko.observable(currBook);
        self.bookInfo = ko.observable(currRequest);
        self.detailInfo = ko.observable({});

        self.GetRoom = function () {
            $.ajax({
                url: url,
                data: { hotelCode: hotelCode, roomId: roomId, roomCode: roomCode },
                type: 'get',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                    self.detailInfo(data);
                }
            });
        };
        
        self.ReserveRoom = function() {
            $('body').block({ message: 'Processing..' });
            var data = currBook;
            $.extend(data, currRequest);
            $.ajax({
                url: urlBook,
                data: data,
                type: 'post',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                },
                complete: function () {
                    $('body').unblock();
                    window.location.href = '/BookRoom';
                }
            });
        };

    }

    var vmodel = new RoomDetailViewModel();
    ko.applyBindings(vmodel);
    vmodel.GetRoom();
});