var hotelboz = {

    homepageInit: function() {        
        $('.search-container input').focus();
    },

    domManipulation: function() {        
    },
    
    initStickyFooter: function() {
      // use waypoint.js. element .sticky-element
      var el = $('.sticky-element');

      // initiation:  set stuck at first time            
      el.addClass('stuck');  
        
      $('.footer').waypoint(function(event,direction){
          el.toggleClass('stuck');          
      },{
          offset:$(window).height()-$('.footer').height()-15
      });
    },

    initColorbox: function() {
        /*
        $(".inline-popup").colorbox({
            inline: true,
            innerWidth: 500
        });
        */
        $('.inline-popup').on('click', function () {
            var theHref = $(this).attr('href');

            jQuery.colorbox({
                inline: true,
                fixed: true,
                href: theHref,
                width: '90%',
                height: '90%'
            });
        });

        $(".youtube").colorbox({
            iframe: true,
            innerWidth: 640,
            innerHeight: 390
        });                
    },


    initCarousel: function() {
        setTimeout(function() {
            
            // HOTEL DETAIL IMAGE SLIDESHOW, USE .slideshow TO HAVE SAME EFFECT
            $('.slideshow').cycle({
                speed: 600,
                manualSpeed: 100,
                fx: 'scrollHorz',
                swipe: true
            });
        }, 100);
       
    },

    roomNumberController: function() {
        // method to control increase/descrease room number
        function priceStringToInt(priceString){
            var newPriceInt = (parseInt(priceString.replace(/[.]+/g,"")));
            return newPriceInt;
        }

        function priceIntToString(priceInt){
            var newPriceString = (priceInt.toString()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
            return newPriceString;
            
        }

    },

    eventCollection: function () {
        $('#menu-button').sidr();

        $('.sidr .has-submenu').on('click', function() {
            $(this).find('.submenu').toggleClass('active');
        });

        function parseDate(dateString) {
            // from D, dd-M-yy to 2015-02-02
            var split1 = dateString.split(" ");
            var theDate = split1[1].split("-");
            var month = ( "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1 ) < 10 ? "0"+( "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1 ) : ( "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1 );
                        
            return(theDate[2]+'-'+month+'-'+theDate[0]);
        }        

        // checkin calendar date picker
        $('#datepicker-checkin').datepicker({
            inline: true,
            altField: '#check-in-date',            
            dateFormat: "D, dd-M-yy",
            minDate: new Date(),
            onSelect: function(dateText, inst) {                    
                    var parsedDate = parseDate(dateText);
                    var defaultCheckoutDate = new Date(parsedDate);
                    
                    defaultCheckoutDate.setDate(defaultCheckoutDate.getDate() + 1);
                    $('#datepicker-checkout').datepicker("option", "minDate", defaultCheckoutDate);
                    
                    // automatically moves to checkout date after done with checkin
                    $('#litebox-checkin-date').removeClass('active');                    
                } 
        });

        $('#check-in-date').change(function(){
            $('#datepicker-checkin').datepicker({
                setDate: $(this).val()              
            });            

        });

        // checkout calendar date picker
        var defaultCheckout = new Date();
        defaultCheckout.setDate(defaultCheckout.getDate() + 1);
        
        $('#datepicker-checkout').datepicker({
            inline: true,
            altField: '#check-out-date',
            dateFormat: "D, dd-M-yy",  
            setDate: defaultCheckout,
            minDate: defaultCheckout,         
            onSelect: function() {
                //close checkout date                
                $('#litebox-checkout-date').removeClass('active');
            }
        });

        $('#check-out-date').change(function(){
            $('#datepicker-checkout').datepicker('setDate', $(this).val());
        });

        /* show datepicker: checkin */
        $('.check-in-container').on('click', function() {                        
            $('#litebox-checkin-date').addClass('active');
            
        });

        /* show datepicker: checkout, */
        $('.check-out-container').on('click', function() {             
            $('#litebox-checkout-date').addClass('active');
            
        });

        // SEARCH AND CLOSE LITEBOX
        $('.litebox-search-hotel').on('focus click', function () {            
            $('#litebox-search-recommendations').addClass('active');
            var theHeight = $('.litebox-slide').innerHeight()-70;            
            $('#litebox-search-recommendations .litebox-body').height(theHeight);
        });

        $('.close-litebox').on('click', function() {
            //$.colorbox.close();
            $(this).parents('.litebox-slide').removeClass('active');
        });
        
        // SORT AND FILTER
        $('.btn-litebox-slide-inline').on('click', function() {
            var elTarget = $(this).data('sourceHref');

            $(elTarget).addClass('active');

            var theHeight = $('.litebox-slide').innerHeight()-70;            
            $('.litebox-body').height(theHeight);
        });

        // readmore
        $('.read-more').on('click', function(e) {
          
          var theText = $(this).parent().find('.text-with-readmore');
          
          theText.toggleClass('expand');


          if ($(this).text() == 'Read more') {            
            $(this).html('Read less');
          } else {
            $(this).html('Read more');
          }

        });

        // anchor animation
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
        });

        // MAKE A DIV/ELEMENT CLICKABLE
        $('.clickable').on('click', function (e) {
            e.preventDefault();
            var url = $(this).find('a').prop('href');

            location.href = url;
        });

        $('.external').on('click', function (e) {
            e.preventDefault();
            var url = $(this).prop('href');

            window.open(url);
        });

        // SORT/FILTER HOTEL. ON CLICK
        $('.sort-item').on('click', function() {
            if ($(this).parent('ul').hasClass('radiobutton')) {
                $(this).parent().find('.check').removeClass('check');
                $(this).addClass('check');
            } else {
                $(this).toggleClass('check');
            }
            
            //$('.close-litebox').click();
            // TODO: SET SOMETHING HERE TO REFLOW THE LIST OF HOTELS
        });

        $('.btn-apply-full').on('click', function() {
            $('.close-litebox').click();
        });

    },

    paymentCombined: function(){

        $('.btn-payment-continue').on('click', function(e) {
            e.preventDefault();

            var elTarget = $(this).parents('.page').index()+1;
            var newMarginLeft = '-'+(elTarget*100)+'%';

            $('.sliding-page-container .inner').css('margin-left', newMarginLeft);
        });

        $('.btn-payment-previous').on('click', function(e) {
            e.preventDefault();

            var elTarget = $(this).parents('.page').index()-1;
            var newMarginLeft = '-'+(elTarget*100)+'%';

            $('.sliding-page-container .inner').css('margin-left', newMarginLeft);
        });
    },

    init: function() {        
        this.homepageInit();
        this.domManipulation(); 
        this.initColorbox();
        //this.initCarousel();   
        this.initStickyFooter();
        this.eventCollection();
        this.roomNumberController();
        this.paymentCombined();        
    }
}

$(document).ready(function() {
    hotelboz.init();
});
