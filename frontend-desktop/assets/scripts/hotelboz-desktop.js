var hotelbozDesktop = {
    initAutocomplete: function() {                        
    },

    homepageInit: function() {        
        
    },

    domManipulation: function() {
        function parseDate(dateString) {
            // from D, dd-M-yy to 2015-02-02
            var split1 = dateString.split(" ");
            var theDate = split1[1].split("-");
            var month = ( "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1 ) < 10 ? "0"+( "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1 ) : ( "JanFebMarAprMayJunJulAugSepOctNovDec".indexOf(theDate[1]) / 3 + 1 );
                        
            return(theDate[2]+'-'+month+'-'+theDate[0]);
        }        
        var today = new Date();
        $('.check-in-out-container').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
            },
            "opens": "center",
            startDate: today,
            minDate: today
            
        });        

    },
    
    initStickyFooter: function() {
      // use waypoint.js. element .sticky-element
      var el = $('.sticky-element');

      // initiation:  set stuck at first time            
      el.addClass('stuck');  
      
      /*var waypoint = new Waypoint({
        element: $('.anchor-sticky'),
        handler: function(e, d) {
          el.toggleClass('stuck');
          console.log('hit');
        },
        offset: 10
      })      */      
      $('.footer').waypoint(function(event,direction){
          el.toggleClass('stuck');          
      },{
          offset:$(window).height()-$('.footer').height()-15
      });
    },

    initColorbox: function() {
        /*
        $(".inline-popup").colorbox({
            inline: true,
            innerWidth: 500
        });
        */
        $('.inline-popup').on('click', function () {
            var theHref = $(this).attr('href');

            jQuery.colorbox({
                inline: true,
                fixed: true,
                href: theHref,
                width: '90%',
                height: '90%'
            });
        });

        $(".youtube").colorbox({
            iframe: true,
            innerWidth: 640,
            innerHeight: 390
        });
               

        $('.close-litebox').on('click', function() {
            $.colorbox.close();
        });
    },

    ajaxForm: function() {
        var options = {
            beforeSubmit:  function() {
                $.colorbox.close();
                window.open("https://play.google.com/store/apps/details?id=com.limoff&hl=en", "googleplay");
            }
        }

        $('#form1').ajaxForm(options);

    },

    initCarousel: function() {
        $('.homepage-slideshow').cycle({
            speed: 1000,
            manualSpeed: 100,
            slides: "> .slideshow-item",
            pager: ".slideshow-paging"
        });     
    },

    roomNumberController: function() {
        // method to control increase/descrease room number
        function priceStringToInt(priceString){
            var newPriceInt = (parseInt(priceString.replace(/[.]+/g,"")));
            return newPriceInt;
        }

        function priceIntToString(priceInt){
            var newPriceString = (priceInt.toString()).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
            return newPriceString;
            
        }


        $('.btn-increase').on('click', function(e) {
            var roomPrice = $(this).parents('.room-item').find('.price-amount').html();

        	var roomNumberValue = parseInt($(this).parent().find('.room-number-field').val(),10);

            var roomMaxValue = parseInt($(this).parent().find('.room-number-field').data('max'),10);

            if(roomNumberValue < roomMaxValue){                
                var roomPriceDivided = priceStringToInt(roomPrice)/roomNumberValue;

                ++roomNumberValue;
                var roomPriceResult = roomPriceDivided*roomNumberValue;

                var roomPriceString = priceIntToString(roomPriceResult);
            }

            $(this).parents('.room-item').find('.price-amount').html(roomPriceString);

        	$(this).parent().find('.room-number-field').attr({
        		"value" : roomNumberValue
        	});
        });

        $('.btn-decrease').on('click', function(e) {
            var roomPrice = $(this).parents('.room-item').find('.price-amount').html();

        	var roomNumberValue = parseInt($(this).parent().find('.room-number-field').val(),10);

            var roomMinValue = 1;

        	
            if(roomNumberValue > roomMinValue){
                var roomPriceInt = priceStringToInt(roomPrice);

                var roomPriceDivided = roomPriceInt/roomNumberValue;

                --roomNumberValue;

                roomPriceResult = roomPriceDivided * roomNumberValue;

                var roomPriceString = priceIntToString(roomPriceResult);
            }

            $(this).parents('.room-item').find('.price-amount').html(roomPriceString);

        	$(this).parent().find('.room-number-field').attr({
        		"value" : roomNumberValue
        	});
        });
    },

    filterAccordion: function() {
        $('.accordion-container .accordion-header').on('click', function() {            
            $(this).parent('.accordion-container').toggleClass('opened');
        });

        //radiobutton and checkbox
        $('.checkbox li').on('click', function() {
            $(this).toggleClass('checked');            
                        
            // TODO: SET SOMETHING HERE TO REFLOW THE LIST OF HOTELS
        });

        $('.radio li').on('click', function() {
            $(this).parent().find('.checked').removeClass('checked');
            $(this).addClass('checked');

            // TODO: SET SOMETHING HERE TO REFLOW THE LIST OF HOTELS
        });
                
        //SORT TAB
        $('.sort-tab li').click(function() {
            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');
        });
    },

    eventCollection: function () {
        // readmore
        $('.read-more').on('click', function(e) {
          
          var theText = $(this).parent().find('.text-with-readmore');
          
          theText.toggleClass('expand');


          if ($(this).text() == 'Read more') {            
            $(this).html('Read less');
          } else {
            $(this).html('Read more');
          }

        });

        // SEARCH RESULT PAGE
        $('.open-change-date').on('click', function() {
            $(this).toggleClass('active');            

             $('.more-search-container').toggleClass('active');
            /*
            if ($(this).hasClass('active')) {
                $('.more-search-container').animate({'height': '90px'}, 'fast');
            } else {
                $('.more-search-container').animate({'height': '0'}, 100);
            }*/
            $(this).parent('div').find('.triangle-container').toggleClass('active');
        });

        // anchor animation
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
        });

        // MAKE A DIV/ELEMENT CLICKABLE
        $('.clickable').on('click', function (e) {
            e.preventDefault();
            var url = $(this).find('a').prop('href');

            location.href = url;
        });

        $('.external').on('click', function (e) {
            e.preventDefault();
            var url = $(this).prop('href');

            window.open(url);
        });

        // SORT/FILTER HOTEL. ON CLICK
        $('.sort-item').on('click', function() {
            $(this).parent().find('.check').removeClass('check');
            $(this).addClass('check');

            $.colorbox.close();
            // TODO: SET SOMETHING HERE TO REFLOW THE LIST OF HOTELS
        });

    },

    paymentCombined: function(){

        $('.btn-payment-continue').on('click', function(e) {
            e.preventDefault();

            var elTarget = $(this).parents('.page').index()+1;
            var newMarginLeft = '-'+(elTarget*100)+'%';

            $('.sliding-page-container .inner').css('margin-left', newMarginLeft);
        });

        $('.btn-payment-previous').on('click', function(e) {
            e.preventDefault();

            var elTarget = $(this).parents('.page').index()-1;
            var newMarginLeft = '-'+(elTarget*100)+'%';

            $('.sliding-page-container .inner').css('margin-left', newMarginLeft);
        });
    },

    init: function() {        
        this.homepageInit();
        this.domManipulation(); 
        this.initColorbox();
        this.initCarousel();
        this.filterAccordion();
        this.initStickyFooter();
        this.eventCollection();
        this.roomNumberController();
        this.paymentCombined();        
    }
}

$(document).ready(function() {
    hotelbozDesktop.init();
});
